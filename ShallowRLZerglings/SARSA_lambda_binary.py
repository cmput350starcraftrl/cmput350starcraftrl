import numpy as np

# see page 250 of 2017 Nov 5 Draft of RL textbook
class SARSA_lambda_binary:
    def __init__(self, num_features, num_actions, action_function, feature_function):
        self.weights = np.zeros((num_actions, num_features))
        self.num_actions = num_actions
        self.num_features = num_features
        self.action_function = action_function
        self.feature_function = feature_function

        self.z = np.zeros(self.weights.shape[1])

        self.alpha = 0.001
        self.lmbda = 0.95
        self.gamma = 1

        self.need_init = True


    def q_hat(self, feat_state):
        max_a = [0]

        for a in range(self.num_actions):
            if np.dot(self.weights[a], feat_state) > np.dot(self.weights[max_a[0]], feat_state):
                max_a = [a]
            elif np.dot(self.weights[a], feat_state) == np.dot(self.weights[max_a[0]], feat_state):
                max_a.append(a)

        if np.random.ranf() < 0.05:
            return np.random.randint(0, self.num_actions)
        return np.random.choice(max_a)

    def init_episode(self, state):
        self.z = np.zeros(self.weights.shape[1])
        self.curr_state = state
        self.curr_action = self.q_hat(self.feature_function(state))

        self.need_init = False

        self.previous_state = np.zeros(self.num_features)
        self.previous_action = 0

    def step(self, R, Sprime, is_terminal):
        delta = R
        for i in self.feature_function(Sprime).nonzero()[0]:
            delta = delta - self.weights[self.curr_action][i]
            self.z[i] = self.z[i] + 1 # accumulating trace

        if is_terminal:
            self.need_init = True
            self.weights[self.curr_action] = self.weights[self.curr_action] + self.alpha * delta * self.z
            return
        Aprime = self.q_hat(self.feature_function(Sprime))

        for i in self.feature_function(Sprime).nonzero()[0]:
            delta = delta + self.gamma * self.weights[Aprime][i]
        self.weights[Aprime] = self.weights[Aprime] + self.alpha * delta * self.z
        self.z = self.gamma * self.lmbda * self.z

        self.curr_state = Sprime
        self.curr_action = Aprime

        return self.action_function(Sprime, Aprime)

    def save_model(self):
        pass