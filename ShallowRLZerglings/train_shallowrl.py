#!/usr/bin/python
# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS-IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Run an agent."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import importlib
import threading

from future.builtins import range  # pylint: disable=redefined-builtin

import numpy as np

from pysc2 import maps
from pysc2.env import available_actions_printer
from pysc2.env import environment
from pysc2.env import run_loop
from pysc2.env import sc2_env

from pysc2.lib import stopwatch

from pysc2.agents import base_agent
from pysc2.lib import actions
from pysc2.lib import features

from absl import logging
from absl import app
from absl import flags

from SARSA_lambda_binary import SARSA_lambda_binary

FLAGS = flags.FLAGS
PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index
PLAYER_FRIENDLY = 1
SELECTED = features.SCREEN_FEATURES.selected.index

MOVE_SCREEN = actions.FUNCTIONS.Move_screen.id
SELECT_ALL = [0]
SELECT_ARMY = actions.FUNCTIONS.select_army.id
SELECT_UNIT = actions.FUNCTIONS.select_unit.id

SELECT_UNIT_ACT_SINGLE = [0]

NOT_QUEUED = [0]

# needed to run agent (modified to fit Baneling and Zergling map)
def set_flags():
    flags.DEFINE_bool("render", False, "Whether to render with pygame.")
    flags.DEFINE_integer("screen_resolution", 84,
                         "Resolution for screen feature layers.")
    flags.DEFINE_integer("minimap_resolution", 64,
                         "Resolution for minimap feature layers.")

    flags.DEFINE_integer("max_agent_steps", 300000, "Total agent steps.")
    flags.DEFINE_integer("game_steps_per_episode", 0, "Game steps per episode.")
    flags.DEFINE_integer("step_mul", 8, "Game steps per agent step.")

    flags.DEFINE_string("agent", "train_shallowrl.SimpleAgent",
                        "Which agent to run")
    flags.DEFINE_enum("agent_race", "T", sc2_env.races.keys(), "Agent's race.")
    flags.DEFINE_enum("bot_race", None, sc2_env.races.keys(), "Bot's race.")
    flags.DEFINE_enum("difficulty", None, sc2_env.difficulties.keys(),
                      "Bot's strength.")

    flags.DEFINE_bool("profile", False, "Whether to turn on code profiling.")
    flags.DEFINE_bool("trace", False, "Whether to trace the code execution.")
    flags.DEFINE_integer("parallel", 1, "How many instances to run in parallel.")

    flags.DEFINE_bool("save_replay", True, "Whether to save a replay at the end.")

    flags.DEFINE_string("map", "DefeatZerglingsAndBanelings", "Name of a map to use.")
    #flags.mark_flag_as_required("map")

    PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index

class STATE_CONSTANTS:
    NUM_UNIT_BITS = 4
    STATE_SIZE = (16 ** 2) * NUM_UNIT_BITS
    EMPTY_BIT = -1 # empty is when all bits 0
    BANELING_BIT = 0
    ZERGLING_BIT = 1
    MARINE_BIT = 2
    CURRENT_BIT = 3

    INT_ZERGLING = 35
    INT_BANELING = 30
    INT_MARINE = 45
    INT_EMPTY = 0

    MAX_NUM_SELECTED = 1
    MAX_NUM_MARINES = 20
    MAX_NUM_ZERGLINGS = 10
    MAX_NUM_BANELINGS = 10

def get_bit_for_unit(unit_type):
    if unit_type == STATE_CONSTANTS.INT_ZERGLING:
        return STATE_CONSTANTS.ZERGLING_BIT
    if unit_type == STATE_CONSTANTS.INT_BANELING:
        return STATE_CONSTANTS.BANELING_BIT
    if unit_type == STATE_CONSTANTS.INT_MARINE:
        return STATE_CONSTANTS.MARINE_BIT
    return STATE_CONSTANTS.EMPTY_BIT

def get_state_approximation(obs):
    unit_state = obs.observation["screen"][8]
    select_state = obs.observation["screen"][SELECTED]

    # bits per coordinate
    bpc = 84
    # bits per unit (encoded x,y coordinates as 84 bits for each)
    bpu = 2 * bpc

    current_pointer = 0
    marine_pointer = bpu * STATE_CONSTANTS.MAX_NUM_SELECTED
    baneling_pointer = marine_pointer + bpu * STATE_CONSTANTS.MAX_NUM_MARINES
    zergling_pointer = baneling_pointer + bpu * STATE_CONSTANTS.MAX_NUM_BANELINGS

    ret_state = np.array([0 for i in range(bpu * (STATE_CONSTANTS.MAX_NUM_MARINES
                                                    + STATE_CONSTANTS.MAX_NUM_BANELINGS
                                                    + STATE_CONSTANTS.MAX_NUM_ZERGLINGS
                                                    + STATE_CONSTANTS.MAX_NUM_SELECTED))])

    # set the 'selected units' bits
    selected_x, selected_y = select_state.nonzero()
    selected_coords = np.array(list(zip(selected_x, selected_y)))
    for sc in range(min(len(selected_coords), STATE_CONSTANTS.MAX_NUM_SELECTED)):
        ret_state[current_pointer + selected_coords[sc][0]] = 1
        ret_state[current_pointer + selected_coords[sc][1] + bpc] = 1
        current_pointer += bpu

    # set the 'zergling unit' bits
    zergling_x, zergling_y = (unit_state == STATE_CONSTANTS.INT_ZERGLING).nonzero()
    zergling_coords = np.array(list(zip(zergling_x, zergling_y)))
    for zc in range(min(len(zergling_coords), STATE_CONSTANTS.MAX_NUM_ZERGLINGS)):
        ret_state[int(zergling_pointer + zergling_coords[zc][0])] = 1
        ret_state[int(zergling_pointer + zergling_coords[zc][1] + bpc)] = 1
        zergling_pointer += bpu

    # set the 'baneling unit' bits
    baneling_x, baneling_y = (unit_state == STATE_CONSTANTS.INT_BANELING).nonzero()
    baneling_coords = np.array(list(zip(baneling_x, baneling_y)))
    for bc in range(min(len(baneling_coords), STATE_CONSTANTS.MAX_NUM_BANELINGS)):
        ret_state[baneling_pointer + baneling_coords[bc][0]] = 1
        ret_state[baneling_pointer + baneling_coords[bc][1] + bpc] = 1
        baneling_pointer += bpu

    # set the 'marine unit' bits
    marine_x, marine_y = (unit_state == STATE_CONSTANTS.INT_MARINE).nonzero()
    marine_coords = np.array(list(zip(marine_x, marine_y)))
    for mc in range(min(len(marine_coords), STATE_CONSTANTS.MAX_NUM_MARINES)):
        ret_state[marine_pointer + marine_coords[mc][0]] = 1
        ret_state[marine_pointer + marine_coords[mc][1] + bpc] = 1
        marine_pointer += bpu

    return ret_state

def get_state_approximation2(unit_state, select_state):
    (unit_state == STATE_CONSTANTS.INT_MARINE)

#sdef action_function(self.state, )

def take_action(obs, action):
    select_state = obs.observation["screen"][SELECTED]
    player_y, player_x = select_state.nonzero()

    try:
        player = (int(player_x.mean()), int(player_y.mean()))
    except:
        return actions.FunctionCall(0, [])
    coord = [player[0], player[1]]
    rew = 0

    if(action == 0): #UP
        if(player[1] >= 16):
            coord = [player[0], player[1] - 16]
        else:
            coord = [player[0], 0]

    elif(action == 1): #DOWN
        if(player[1] <= 83 - 16):
            coord = [player[0], player[1] + 16]
        else:
            coord = [player[0], 83]

    elif(action == 2): #LEFT
        if(player[0] >= 16):
            coord = [player[0] - 16, player[1]]
        else:
            coord = [0, player[1]]

    elif(action == 3): #RIGHT
        if(player[0] <= 83 - 16):
            coord = [player[0] + 16, player[1]]
        else:
            coord = [83, player[1]]

    new_action = actions.FunctionCall(MOVE_SCREEN, [NOT_QUEUED, coord])

    return new_action

class SimpleAgent(base_agent.BaseAgent):
    def __init__(self):
        self.episode_rewards = []

        base_agent.BaseAgent.__init__(self)
        feat_size = 84*2 * (STATE_CONSTANTS.MAX_NUM_MARINES
                            + STATE_CONSTANTS.MAX_NUM_BANELINGS
                            + STATE_CONSTANTS.MAX_NUM_ZERGLINGS
                            + STATE_CONSTANTS.MAX_NUM_SELECTED)

        act_size = 4
        self.learner = SARSA_lambda_binary(feat_size, act_size, take_action, get_state_approximation)
        self.curr_unit = 0
        self.bool_selected = False
        self.bool_last_action_unit = False
        self.to_move = False
        self.action = actions.FunctionCall(0, [])

    def step(self, obs):
        base_agent.BaseAgent.step(self, obs)

        is_terminal = obs.step_type == environment.StepType.LAST

        if (self.learner.need_init):
            logging.info("WE INITED")
            self.learner.init_episode(obs)
            self.episode_rewards.append(0.0)

        player_relative = obs.observation["screen"][PLAYER_RELATIVE]

        if is_terminal:
            logging.info("WE TERMINATED")
            base_agent.BaseAgent.reset(self)
            rew = obs.reward
            #if rew == 0:
            #    rew = -1
            #if rew > 0:
            #    rew = 100
            self.episode_rewards[-1] += obs.reward
            logging.info(str(self.episode_rewards))
            self.learner.step(rew, obs, is_terminal)
            return actions.FunctionCall(0, [])

        if self.bool_last_action_unit:
            rew = obs.reward
            #if rew > 0:
            #    rew = 100
            self.episode_rewards[-1] += obs.reward
            self.action = self.learner.step(rew, obs, is_terminal)

        if MOVE_SCREEN not in obs.observation["available_actions"] or (self.bool_selected and not self.to_move):
            # if no one is selected
            self.bool_selected = False
            self.bool_last_action_unit = False
            return actions.FunctionCall(SELECT_ARMY, [SELECT_ALL])

        if SELECT_UNIT in obs.observation["available_actions"]:
            select_state = obs.observation["screen"][SELECTED]
            self.curr_unit += 1
            self.curr_unit = int((self.curr_unit) % (select_state.nonzero()[0].shape[0] / 9))

            self.bool_last_action_unit = False
            self.bool_selected = True
            self.to_move = True

            return actions.FunctionCall(SELECT_UNIT, [SELECT_UNIT_ACT_SINGLE, [self.curr_unit]])

        self.bool_last_action_unit = True

        self.to_move = False
        return self.action



# needed to run agent (no modifications)
def run_thread(agent_cls, map_name, visualize):
    with sc2_env.SC2Env(
            map_name=map_name,
            agent_race=FLAGS.agent_race,
            bot_race=FLAGS.bot_race,
            difficulty=FLAGS.difficulty,
            step_mul=FLAGS.step_mul,
            game_steps_per_episode=FLAGS.game_steps_per_episode,
            screen_size_px=(FLAGS.screen_resolution, FLAGS.screen_resolution),
            minimap_size_px=(FLAGS.minimap_resolution, FLAGS.minimap_resolution),
            visualize=visualize) as env:
        env = available_actions_printer.AvailableActionsPrinter(env)
        agent = agent_cls()
        run_loop.run_loop([agent], env, FLAGS.max_agent_steps)
        if FLAGS.save_replay:
            env.save_replay(agent_cls.__name__)

# needed to run agent (no modifications)
def main(unused_argv):
    """Run an agent."""
    stopwatch.sw.enabled = FLAGS.profile or FLAGS.trace
    stopwatch.sw.trace = FLAGS.trace

    maps.get(FLAGS.map)  # Assert the map exists.

    agent_module, agent_name = FLAGS.agent.rsplit(".", 1)
    agent_cls = getattr(importlib.import_module(agent_module), agent_name)

    threads = []
    for _ in range(FLAGS.parallel - 1):
        t = threading.Thread(target=run_thread, args=(agent_cls, FLAGS.map, False))
        threads.append(t)
        t.start()

    run_thread(agent_cls, FLAGS.map, FLAGS.render)

    for t in threads:
        t.join()

    if FLAGS.profile:
        print(stopwatch.sw)

# needed to run agent (no modifications)
def entry_point():  # Needed so setup.py scripts work.
    app.run(main)

if __name__ == "__main__":
    set_flags()
    app.run(main)